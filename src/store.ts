import { initAction } from "./initAction";
import { IAction, IStore, Subscription } from "./types";
import { IReducer } from "./types/iReducer.type";

enum Phase { dispatch, dispense, idle }

export class Store<T> implements IStore<T> {
  private state: T;
  private phase: Phase = Phase.idle;
  private subscriptions: Set<Subscription<T>> = new Set();

  constructor(private reducer: IReducer<T>, initialState?: T) {
    this.state = initialState;
    if (this.state == null) { this.dispatch(initAction); }
  }

  public dispatch = (action: IAction) => {
    if (this.phase !== Phase.idle) {
      throw new Error("Can't dispatch inside reducer or subscriptions");
    }
    this.phase = Phase.dispatch;
    this.state = this.reducer(this.state, action);
    this.phase = Phase.dispense;
    for (let subscription of Array.from(this.subscriptions)) {
      subscription(this.state);
    }
    this.phase = Phase.idle;
  }

  public getState = () => {
    if (this.phase === Phase.dispatch) {
      throw new Error("Can't get state from inside reducers");
    }

    return this.state;
  }

  public subscribe = (subscription: Subscription<T>) => {
    this.subscriptions.add(subscription);
    return () => this.subscriptions.delete(subscription);
  }

  public dispose = () => {
    this.reducer = null;
    this.subscriptions = null;
  }
}
