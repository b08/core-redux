import { IReducer } from "./types/iReducer.type";
import { IStore } from "./types";
import { Store } from "./store";

export function createStore<T>(reducer: IReducer<T>, initialState?: T): IStore<T> {
  return new Store<T>(reducer, initialState);
}
