import { IAction } from "./iAction.type";

export type Unsubscribe = () => void;
export type Subscription<T> = (state: T) => void;

export interface IStore<T = any> {
  dispatch(action: IAction): void;
  subscribe(subscription: Subscription<T>): Unsubscribe;
  getState(): T;
  dispose(): void;
}
