import { IAction } from "./iAction.type";

export type IReducer<T> = (prev: T, action: IAction) => T;
