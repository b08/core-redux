import { IAction } from "./types";

export const initAction: IAction = {
  type: "@@INIT"
};
