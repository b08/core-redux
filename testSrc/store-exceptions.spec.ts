import { test } from "@b08/test-runner";
import { createStore } from "../src";

test("store throws an exception for subscription dispatch", expect => {
  // arrange
  const store = createStore(() => 1);
  store.subscribe(() => store.dispatch({ type: "action2" }));
  // act
  expect.exception(() => {
    store.dispatch({ type: "action1" });
  });

  // assert
});

test("store throws an exception for reducer dispatch", expect => {
  // arrange
  const store = createStore(val => {
    if (!val) { return 1; }
    store.dispatch({ type: "action2" });
    return 2;
  });

  // act
  expect.exception(() => {
    store.dispatch({ type: "action1" });
  });

  // assert
});

test("store throws an exception for reducer getState", expect => {
  // arrange
  const store = createStore(val => {
    if (!val) { return 1; }
    store.getState();
    return 2;
  });

  // act
  expect.exception(() => {
    store.dispatch({ type: "action1" });
  });

  // assert
});

test("store throws an exception after dispose", expect => {
  // arrange
  let called = false;
  const store = createStore(val => {
    if (!val) { return false; }
    return called = true;
  });
  store.subscribe(() => called = true);

  // act
  store.dispose();
  expect.exception(() => {
    store.dispatch({ type: "action1" });
  });

  // assert
  expect.false(called);
});
