import { test } from "@b08/test-runner";
import { createStore, IAction } from "../src";

const defaultState = 10;
let lastAction;

function testReducer(prev: number, action: IAction): number {
  if (prev == null) { return defaultState; }
  lastAction = action;
  return prev + 1;
}

test("store runs initial action", expect => {
  // arrange

  // act
  const store = createStore(testReducer);
  const state = store.getState();

  // assert
  expect.equal(state, defaultState);
});

test("store sets predefined state", expect => {
  // arrange

  // act
  const store = createStore(testReducer, 5);
  const state = store.getState();

  // assert
  expect.equal(state, 5);
});

test("store allow subscriptions", expect => {
  // arrange
  let subscribedState: number;
  const store = createStore(testReducer, 5);
  store.subscribe(state => subscribedState = state);
  const action = { type: "action" };

  // act
  store.dispatch(action);
  const state = store.getState();

  // assert
  expect.equal(subscribedState, 6);
  expect.equal(state, 6);
  expect.equal(action, lastAction);
});

test("store allow unsubscribe", expect => {
  // arrange
  let subscribedState: number = 0;
  const store = createStore(testReducer, 5);
  const unsubscribe = store.subscribe(state => subscribedState = state);
  unsubscribe();
  const action = { type: "action" };

  // act
  store.dispatch(action);
  const state = store.getState();

  // assert
  expect.equal(subscribedState, 0);
  expect.equal(state, 6);
  expect.equal(action, lastAction);
});

